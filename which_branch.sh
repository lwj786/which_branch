#!/bin/bash

color_of_modified="\033[0;31m"
color_of_staged="\033[0;32m"
color_of_both="\033[1;33m"
background_color_of_ahead="\033[1;45m"
background_color_of_behind="\033[0;40m"

clear_color="\033[00m"

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    echo -e "前景色表示文件状态："$color_of_modified"modified"$clear_color", "$color_of_staged"staged"$clear_color", "$color_of_both"both"$clear_color""
    echo -e "背景色表示提交状态："$background_color_of_ahead"ahead"$clear_color", "$background_color_of_behind"behind"$clear_color""
    exit
fi

# 判断
git_status=`git status 2>> /dev/null`
if [ -n "$git_status" ]; then
    current_branch=`git status | awk 'NR==1 {print}' | awk '{print $NF}'`

    if [ -n "`echo "$git_status" | grep "git add"`" ]; then
        has_modified="True"
    fi

    if [ -n "`echo "$git_status" | grep "git reset HEAD "`" ]; then
        has_staged="True"
    fi

    if [ -n "`echo "$git_status" | grep "git push"`" ]; then
        is_ahead="True"
    elif [ -n "`echo "$git_status" | grep "git pull"`" ]; then
        is_behind="True"
    fi
else
    exit
fi

# 前景色
color="\033[1;37m"
if [ -n "$has_modified" -a -n "$has_staged" ]; then
    color=$color_of_both
elif [ -n "$has_modified" ]; then
    color=$color_of_modified
elif [ -n "$has_staged" ]; then
    color=$color_of_staged
fi

#背景色
if [ -n "$is_ahead" ]; then
    background_color=$background_color_of_ahead
elif [ -n "$is_behind" ]; then
    background_color=$background_color_of_behind
fi

#组合颜色
if [ -n "$background_color" ]; then
    color="${color%'m'}""${background_color:6}"
fi

echo -e "$color""(""$current_branch"")""$clear_color"" "